package com.mba.inverter;

import com.mba.MPPTracker.Controller_MySQL;
import com.mba.inverter.Structs.StInverter;
import com.mba.inverter.Structs.StInverterList;

public class T001_StInverterList {

	

	//Test schreiben
	public static void main(String[] args) {		
		try {
			StInverterList list = new StInverterList();
			StInverter newVal = new StInverter();
			newVal.setValue(1000, 200, 5000, 2300, 50, 1, 2);
			list.add(newVal);
			
			newVal = new StInverter();
			newVal.setValue(2000, 400, 10000, 2500, 100, 3, 4);
			list.add(newVal);
			
			Controller_MySQL.connectionIp = "192.168.178.210";
			Controller_MySQL mysql = new Controller_MySQL();
			mysql.addValue(list);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
