package com.mba.inverter;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fazecast.jSerialComm.SerialPort;
import com.mba.MPPTracker.Controller_MySQL;
import com.mba.MPPTracker.ModbusRTU;
import com.mba.MPPTracker.PausableExecutor;
import com.mba.Tasmota.StSensor;
import com.mba.inverter.Structs.StInverter;
import com.mba.inverter.Structs.StInverterList;

public class Controller_Modbus extends Thread {
	private static Logger logger = LoggerFactory.getLogger(Controller_Modbus.class);
	
	private ModbusRTU modbusRTU;
	private StInverterList inverterList;
	
	private IMqttClient mqttClient = null;
	private MqttConnectOptions mqttOptions = null;
	
	private static String comPort = "/dev/ttyUSB0";
	private static String comPortName = "ttyUSB0";
	
	PausableExecutor executor = new PausableExecutor(1);
	public Controller_Modbus(StInverterList inverterList, MqttConnectOptions mqttOptions) throws Exception {
		try {
			SerialPort[] ports = SerialPort.getCommPorts();
			boolean foundPort = false;
			for (SerialPort port: ports) {
				if(port.getSystemPortName().equals(comPortName)) {
					foundPort = true;
				}
			    System.out.println(port.getSystemPortName());
			}
		} catch (Exception e) {
			e.printStackTrace();
//			throw e;
		}
		
		this.inverterList = inverterList;
		this.mqttOptions = mqttOptions;
	}

	
	private void modbusConnect() throws Exception {
//		logger.info("ModbusRTU init...");
		modbusRTU = new ModbusRTU();
		modbusRTU.connect(comPort, 9600, 0, 8, 1);
//			modbusRTU.connect("COM3", 9600, 0, 8, 1);
	}
	
	private void modbusDisconnect() {
		modbusRTU.disconnect();
	}
	static boolean enableLog = false;
	
	public void run() {
		while(true) {
			try {
				if(mqttClient == null || !mqttClient.isConnected()) {
					logger.info("MQTT Connect...");
					mqttClient = new MqttClient("tcp://192.168.178.210:1883", "Raspberry");
					mqttClient.connect(mqttOptions);
					subscribeMqtt();
				}
			}  catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			try {
				modbusConnect();
				logger.debug("executor resume...");
				try {
					executor.resume();
				} catch (Exception e) {
					
				}
				try {
					Thread.sleep(10*1000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					if(enableLog)
						e1.printStackTrace();
				}
				
				try {
					executor.pause();
				} catch (Exception e) {
					
				}
				logger.debug("start update...");
				StInverter value = new StInverter();
				value.update(modbusRTU);
				value.debug();
				
				synchronized (inverterList) {
					inverterList.add(value);
					sendMqtt(value);
				}
				
				modbusDisconnect();
			} catch (Exception e) {
				if(enableLog)
					e.printStackTrace();
				try {
					Thread.sleep(300*1000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					if(enableLog)
						e1.printStackTrace();
				}
			}
			try {
				Thread.sleep(10*1000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				if(enableLog)
					e1.printStackTrace();
			}
		}
	}

	private void subscribeMqtt() throws MqttException {
		if(mqttClient != null) {
			mqttClient.setCallback(new MqttCallback() {
				ObjectMapper mapper = new ObjectMapper();
				public void connectionLost(Throwable cause) {
					System.out.println("connectionLost: " + cause.getMessage());
				}
		
				public void messageArrived(String topic, MqttMessage message) {
					
					
					try {
						String[] topicSplit = topic.split("/");
						if(topicSplit[0].equals("tele")) {
							String macAddr = topicSplit[1];
							if(topicSplit[2].equals("SENSOR")) {
								StSensor newVal = mapper.readValue(message.getPayload(), StSensor.class);
								Controller_MySQL mysql = new Controller_MySQL();
								mysql.addValue(newVal, macAddr);
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
						
						logger.error("topic: " + topic);
						logger.error("Qos: " + message.getQos());
						logger.error("message content: " + new String(message.getPayload()));
					}
				}
				
				public void deliveryComplete(IMqttDeliveryToken token) {
					//System.out.println("deliveryComplete---------" + token.isComplete());
				}
			});
			mqttClient.subscribe("#", 0);
		}
	}
	
	public void sendMqtt(StInverter value) throws MqttPersistenceException, MqttException {
		try {
			if (mqttClient == null || !mqttClient.isConnected()) {
		        return;
		    }
			byte[] payload = String.format("%4.1f",((float)value.getVac1()) / 10).getBytes();
		    MqttMessage msg = new MqttMessage(payload);
		    msg.setQos(0);
		    msg.setRetained(false);
		    mqttClient.publish("/gartenhaus/pv/voltage", msg);
		    
		    payload = String.format("%4.1f",((float)value.getPV1Watt()) / 10).getBytes();
		    msg = new MqttMessage(payload);
		    msg.setQos(0);
		    msg.setRetained(false);
		    mqttClient.publish("/gartenhaus/pv/power", msg);
	
		    payload = String.format("%4.1f",((float)value.getIac1()) / 10).getBytes();
		    msg = new MqttMessage(payload);
		    msg.setQos(0);
		    msg.setRetained(false);
		    mqttClient.publish("/gartenhaus/pv/amps", msg);
		    
		    payload = String.format("%4.1f",((float)value.getEnergy_today()) / 10).getBytes();
		    msg = new MqttMessage(payload);
		    msg.setQos(0);
		    msg.setRetained(false);
		    mqttClient.publish("/gartenhaus/pv/energyToday", msg);
		    
		    payload = String.format("%4.1f",((float)value.getEnergy_total()) / 10).getBytes();
		    msg = new MqttMessage(payload);
		    msg.setQos(0);
		    msg.setRetained(false);
		    mqttClient.publish("/gartenhaus/pv/energyTotal", msg);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void closeMqtt() {
		try{
			mqttClient.disconnect();
			mqttClient.close();
		}  catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
