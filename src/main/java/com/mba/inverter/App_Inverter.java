package com.mba.inverter;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

import com.mba.Tasmota.StTasmotaList;
import com.mba.inverter.Structs.StInverterList;
import com.mba.inverter.Structs.Thread_03_WriteData;
import com.mba.inverter.Structs.Thread_04_FritzBox;

@SpringBootApplication
public class App_Inverter {
	private static Logger logger = LoggerFactory.getLogger(App_Inverter.class);

	private static Controller_Modbus modbus = null;
	private static StInverterList inverterList = new StInverterList();
	private static StTasmotaList tasmotaList = new StTasmotaList();
	
	public static void main(String[] args) {
		System.setProperty("spring.devtools.restart.enabled", "false");
		
		MqttConnectOptions mqttOptions = new MqttConnectOptions();
		mqttOptions.setAutomaticReconnect(true);
		mqttOptions.setUserName("ESP8266Client");
		mqttOptions.setPassword("USER".toCharArray());
		
		
//		try {
//			modbus = new Controller_Modbus(inverter);
//			modbus.start();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		try {
			modbus = new Controller_Modbus(inverterList, mqttOptions);
			modbus.start();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		SpringApplication.run(App_Inverter.class, args);
	}

	

	@EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) {
		logger.info("START!!");
		SchedulerFactory sf = new StdSchedulerFactory();
		
		Scheduler sched = null;
		try {
			sched = sf.getScheduler();
			
			JobDetail job_tracker = JobBuilder.newJob(Thread_03_WriteData.class)
				    .withIdentity("job_inverter_write_all_data", "15min")
				    .build();
			
			Trigger trigger_15min = TriggerBuilder.newTrigger()
				    .withIdentity("crontrigger", "15min")
				    .withSchedule(CronScheduleBuilder.cronSchedule("0 0/15 * ? * * *"))
				    .build();
			
			sched.scheduleJob(job_tracker, trigger_15min);
			
			
			
			JobDetail job_tracker5 = JobBuilder.newJob(Thread_04_FritzBox.class)
				    .withIdentity("job_fritzbox", "5min")
				    .build();
			
			Trigger trigger_5min = TriggerBuilder.newTrigger()
				    .withIdentity("crontrigger", "5min")
				    .withSchedule(CronScheduleBuilder.cronSchedule("0 0/5 * ? * * *"))
				    .build();
			
			sched.scheduleJob(job_tracker5, trigger_5min);
			
			
			sched.start();
		} catch (SchedulerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
    }

	public static StInverterList getInverterList() {
		return inverterList;
	}


	

	public static StTasmotaList getTasmotaList() {
		return tasmotaList;
	}
}
