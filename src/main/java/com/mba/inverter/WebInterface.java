package com.mba.inverter;

import java.util.LinkedList;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mba.MPPTracker.Controller_MySQL;
import com.mba.inverter.Structs.StInverterList;
import com.mba.inverter.Structs.StInverterStatistic;

@Controller
public class WebInterface {	
	@ResponseBody
	@RequestMapping(value = "/api_pv/inverter/tracker", method = RequestMethod.GET)
	public StInverterList pv_tracker()
	{
		 return App_Inverter.getInverterList();
	}
	
	@ResponseBody
	@RequestMapping(value = "/api_pv/inverter/tracker/hist", method = RequestMethod.GET)
	public LinkedList<StInverterStatistic> pv_tracker_hist()
	{
		 return new Controller_MySQL().getInverterLastValues();
	}
}
