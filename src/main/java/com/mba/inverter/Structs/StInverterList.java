package com.mba.inverter.Structs;

import java.util.LinkedList;
import java.util.OptionalDouble;

public class StInverterList {
	private LinkedList<StInverter> valueList = new LinkedList<StInverter>();
	
	public Integer getMinVoltage() {
		OptionalDouble val = valueList.stream().map(c -> c.getPV1V()).mapToDouble(a -> a).min();
		if(val.isPresent()) {
			return (int) val.getAsDouble();
		} else {
			return null;
		}
	}
	
	public Integer getMaxVoltage() {
		OptionalDouble val = valueList.stream().map(c -> c.getPV1V()).mapToDouble(a -> a).max();
		if(val.isPresent()) {
			return (int) val.getAsDouble();
		} else {
			return null;
		}
	}
	
	public Integer getAvgVoltage() {
		OptionalDouble val = valueList.stream().map(c -> c.getPV1V()).mapToDouble(a -> a).average();
		if(val.isPresent()) {
			return (int) val.getAsDouble();
		} else {
			return null;
		}
	}
	
	
	
	public Integer getMinCurrent() {
		OptionalDouble val = valueList.stream().map(c -> c.getPV1Curr()).mapToDouble(a -> a).min();
		if(val.isPresent()) {
			return (int) val.getAsDouble();
		} else {
			return null;
		}
	}
	
	public Integer getMaxCurrent() {
		OptionalDouble val = valueList.stream().map(c -> c.getPV1Curr()).mapToDouble(a -> a).max();
		if(val.isPresent()) {
			return (int) val.getAsDouble();
		} else {
			return null;
		}
	}
	
	public Integer getAvgCurrent() {
		OptionalDouble val = valueList.stream().map(c -> c.getPV1Curr()).mapToDouble(a -> a).average();
		if(val.isPresent()) {
			return (int) val.getAsDouble();
		} else {
			return null;
		}
	}
	

	public Integer getMinPower() {
		OptionalDouble val = valueList.stream().map(c -> c.getPV1Watt()).mapToDouble(a -> a).min();
		if(val.isPresent()) {
			return (int) val.getAsDouble();
		} else {
			return null;
		}
	}
	public Integer getMaxPower() {
		OptionalDouble val = valueList.stream().map(c -> c.getPV1Watt()).mapToDouble(a -> a).max();
		if(val.isPresent()) {
			return (int) val.getAsDouble();
		} else {
			return null;
		}
	}
	public Integer getAvgPower() {
		OptionalDouble val = valueList.stream().map(c -> c.getPV1Watt()).mapToDouble(a -> a).average();
		if(val.isPresent()) {
			return (int) val.getAsDouble();
		} else {
			return null;
		}
	}
	
	
	
	public Integer getMinVac1() {
		OptionalDouble val = valueList.stream().map(c -> c.getVac1()).mapToDouble(a -> a).min();
		if(val.isPresent()) {
			return (int) val.getAsDouble();
		} else {
			return null;
		}
	}
	public Integer getMaxVac1() {
		OptionalDouble val = valueList.stream().map(c -> c.getVac1()).mapToDouble(a -> a).max();
		if(val.isPresent()) {
			return (int) val.getAsDouble();
		} else {
			return null;
		}
	}
	public Integer getAvgVac1() {
		OptionalDouble val = valueList.stream().map(c -> c.getVac1()).mapToDouble(a -> a).average();
		if(val.isPresent()) {
			return (int) val.getAsDouble();
		} else {
			return null;
		}
	}
	
	public Integer getMinIac1() {
		OptionalDouble val = valueList.stream().map(c -> c.getIac1()).mapToDouble(a -> a).min();
		if(val.isPresent()) {
			return (int) val.getAsDouble();
		} else {
			return null;
		}
	}
	public Integer getMaxIac1() {
		OptionalDouble val = valueList.stream().map(c -> c.getIac1()).mapToDouble(a -> a).max();
		if(val.isPresent()) {
			return (int) val.getAsDouble();
		} else {
			return null;
		}
	}
	public Integer getAvgIac1() {
		OptionalDouble val = valueList.stream().map(c -> c.getIac1()).mapToDouble(a -> a).average();
		if(val.isPresent()) {
			return (int) val.getAsDouble();
		} else {
			return null;
		}
	}

	public int getEnergy_today() {
		return valueList.getLast().getEnergy_today();
	}
	public int getEnergy_total() {
		return valueList.getLast().getEnergy_total();
	}
	public int getModbusAdress() {
		return valueList.getLast().getModbusAdress();
	}

	public int size() {
		return valueList.size();
	}

	public void clear() {
		valueList.clear();
	}

	public void add(StInverter value) {
		valueList.add(value);
	}

	public LinkedList<StInverter> getValueList() {
		return valueList;
	}
}
