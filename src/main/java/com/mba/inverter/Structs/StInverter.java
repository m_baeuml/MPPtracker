package com.mba.inverter.Structs;

import java.sql.ResultSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mba.MPPTracker.ModbusRTU;
import com.mba.MPPTracker.Structs.StMPPTracker;
import com.serotonin.modbus4j.code.DataType;
import com.serotonin.modbus4j.code.RegisterRange;

public class StInverter {
	private static Logger logger = LoggerFactory.getLogger(StMPPTracker.class);
	
	private int modbusAdress				= 1;
	
//	private int Ppv				= 0;
	private int PV1V			= 0;
	private int PV1Curr			= 0;
	private int PV1Watt			= 0;
	
	private int Vac1			= 0;
	private int Iac1			= 0;
	
	private int energy_today 	= 0;
	private int energy_total	= 0;
	
	public StInverter() {
		
	}

	public void setValue(int PV1V, int PV1Curr, int PV1Watt, int Vac1, int Iac1, int energy_today, int energy_total) {
		this.PV1V = PV1V;
		this.PV1Curr = PV1Curr;
		this.PV1Watt = PV1Watt;
		
		this.Vac1 = Vac1;
		this.Iac1 = Iac1;
		
		this.energy_today = energy_today;
		this.energy_total = energy_total;
	}
	
	public void update(ModbusRTU modbusRTU) throws Exception {
//		Ppv = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 1, DataType.FOUR_BYTE_INT_UNSIGNED).intValue();
		
		PV1V = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 3, DataType.TWO_BYTE_INT_UNSIGNED).intValue();
		PV1Curr = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 4, DataType.TWO_BYTE_INT_UNSIGNED).intValue();
		PV1Watt = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 5, DataType.FOUR_BYTE_INT_UNSIGNED).intValue();
		
		Vac1 = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 38, DataType.TWO_BYTE_INT_UNSIGNED).intValue();
		Iac1 = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 39, DataType.TWO_BYTE_INT_UNSIGNED).intValue();
		
		energy_today = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 53, DataType.FOUR_BYTE_INT_UNSIGNED).intValue();
		energy_total = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 55, DataType.FOUR_BYTE_INT_UNSIGNED).intValue();
	}
	
	public void debug() {
		logger.debug(modbusAdress + "\t" + "PV1" + "\t" + "V" + "\t" + PV1V);
		logger.debug(modbusAdress + "\t" + "PV1" + "\t" + "A" + "\t" + PV1Curr);
		logger.debug(modbusAdress + "\t" + "PV1" + "\t" + "W" + "\t" + PV1Watt);
		logger.debug("");
		logger.debug(modbusAdress + "\t" + "Grid" + "\t" + "V" + "\t" + Vac1);
		logger.debug(modbusAdress + "\t" + "Grid" + "\t" + "A" + "\t" + Iac1);
		logger.debug("");
		logger.debug(modbusAdress + "\t" + "Energy" + "\t" + "Today W" + "\t" + energy_today);
		logger.debug(modbusAdress + "\t" + "Energy" + "\t" + "Total W" + "\t" + energy_total);
		logger.debug("");
	}

	public int getModbusAdress() {
		return modbusAdress;
	}

	public int getPV1V() {
		return PV1V;
	}

	public int getPV1Curr() {
		return PV1Curr;
	}

	public int getPV1Watt() {
		return PV1Watt;
	}

	public int getVac1() {
		return Vac1;
	}

	public int getIac1() {
		return Iac1;
	}

	public int getEnergy_today() {
		return energy_today;
	}

	public int getEnergy_total() {
		return energy_total;
	}

	
}
