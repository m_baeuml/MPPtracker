package com.mba.inverter.Structs;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.mba.MPPTracker.Controller_MySQL;
import com.mba.inverter.App_Inverter;


public class Thread_03_WriteData implements Job  {
	private static Logger logger = LoggerFactory.getLogger(Thread_03_WriteData.class);
	
	public void execute(JobExecutionContext context) throws JobExecutionException {
		logger.info("starting...");
		try {
			Controller_MySQL mysql = new Controller_MySQL();
			mysql.addValue(App_Inverter.getInverterList());
			
		} catch (Exception e) {
			logger.error(e.getClass().getName() + " " + e.getMessage());
			e.printStackTrace();
		}
		logger.info("done");
	}
}
