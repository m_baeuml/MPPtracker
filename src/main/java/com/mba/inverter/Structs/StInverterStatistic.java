package com.mba.inverter.Structs;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class StInverterStatistic extends StInverter {
	private Date createdate = null;
	
	private int pv_voltage_min = 0;
	private int pv_voltage_avg = 0;
	private int pv_voltage_max = 0;
	
	private int pv_current_min = 0;
	private int pv_current_avg = 0;
	private int pv_current_max = 0;
	
	private int pv_power_min = 0;
	private int pv_power_avg = 0;
	private int pv_power_max = 0;
	
	private int ac_voltage_min = 0;
	private int ac_voltage_avg = 0;
	private int ac_voltage_max = 0;
	
	private int ac_current_min = 0;
	private int ac_current_avg = 0;
	private int ac_current_max = 0;
	
	private int energyToday = 0;
	private int energyTotal = 0;
	
	public StInverterStatistic(ResultSet rs) throws SQLException {
		//pv_voltage_min, pv_voltage_avg, pv_voltage_max, 
		//pv_current_min, pv_current_avg, pv_current_max, 
		//pv_power_min, pv_power_avg, pv_power_max, 
		//ac_voltage_min, ac_voltage_avg, ac_voltage_max, 
		//ac_current_min, ac_current_avg, ac_current_max, energyToday, energyTotal
		
		this.pv_voltage_min = rs.getInt("pv_voltage_min");
		this.pv_voltage_avg = rs.getInt("pv_voltage_avg");
		this.pv_voltage_max = rs.getInt("pv_voltage_max");
		
		this.pv_current_min = rs.getInt("pv_current_min");
		this.pv_current_avg = rs.getInt("pv_current_avg");
		this.pv_current_max = rs.getInt("pv_current_max");
		
		this.pv_power_min = rs.getInt("pv_power_min");
		this.pv_power_avg = rs.getInt("pv_power_avg");
		this.pv_power_max = rs.getInt("pv_power_max");
		
		this.ac_voltage_min = rs.getInt("ac_voltage_min");
		this.ac_voltage_avg = rs.getInt("ac_voltage_avg");
		this.ac_voltage_max = rs.getInt("ac_voltage_max");
		
		this.ac_current_min = rs.getInt("ac_current_min");
		this.ac_current_avg = rs.getInt("ac_current_avg");
		this.ac_current_max = rs.getInt("ac_current_max");
		

		this.energyToday = rs.getInt("energyToday");
		this.energyTotal = rs.getInt("energyTotal");
		
		createdate = rs.getTimestamp("createdate");
	}

	public int getPv_voltage_min() {
		return pv_voltage_min;
	}

	public int getPv_voltage_avg() {
		return pv_voltage_avg;
	}

	public int getPv_voltage_max() {
		return pv_voltage_max;
	}

	public int getPv_current_min() {
		return pv_current_min;
	}

	public int getPv_current_avg() {
		return pv_current_avg;
	}

	public int getPv_current_max() {
		return pv_current_max;
	}

	public int getPv_power_min() {
		return pv_power_min;
	}

	public int getPv_power_avg() {
		return pv_power_avg;
	}

	public int getPv_power_max() {
		return pv_power_max;
	}

	public int getAc_voltage_min() {
		return ac_voltage_min;
	}

	public int getAc_voltage_avg() {
		return ac_voltage_avg;
	}

	public int getAc_voltage_max() {
		return ac_voltage_max;
	}

	public int getAc_current_min() {
		return ac_current_min;
	}

	public int getAc_current_avg() {
		return ac_current_avg;
	}

	public int getAc_current_max() {
		return ac_current_max;
	}

	public int getEnergyToday() {
		return energyToday;
	}

	public int getEnergyTotal() {
		return energyTotal;
	}

	public Date getCreatedate() {
		return createdate;
	}	
}
