package com.mba.inverter.Structs;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mba.MPPTracker.Controller_MySQL;
import com.mba.fritzBox.FritzBoxResponse;


public class Thread_04_FritzBox implements Job  {
	private static Logger logger = LoggerFactory.getLogger(Thread_04_FritzBox.class);
	
	public void execute(JobExecutionContext context) throws JobExecutionException {
//		logger.info("starting...");
		try {
			Controller_MySQL mysql = new Controller_MySQL();
			FritzBoxResponse fritzBox = new FritzBoxResponse("fritz.box");
			mysql.addValue(fritzBox.getValue());
		} catch (Exception e) {
			logger.error(e.getClass().getName() + " " + e.getMessage());
			e.printStackTrace();
		}
//		logger.info("done");
	}
}
