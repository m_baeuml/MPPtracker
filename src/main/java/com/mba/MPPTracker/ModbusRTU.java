package com.mba.MPPTracker;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fazecast.jSerialComm.SerialPort;
import com.serotonin.modbus4j.ModbusFactory;
import com.serotonin.modbus4j.ModbusMaster;
import com.serotonin.modbus4j.code.DataType;
import com.serotonin.modbus4j.code.RegisterRange;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.locator.BaseLocator;
import com.serotonin.modbus4j.serial.SerialPortWrapper;

/**
 * Hello world!
 *
 */
public class ModbusRTU 
{
	
	private static Logger logger = LoggerFactory.getLogger(ModbusRTU.class);
	
    public static void main( String[] args ) throws InterruptedException, ExecutionException, ModbusInitException
    {
    	logger.info( "Hello World!" );
    	
        ModbusRTU inst = new ModbusRTU();
        try {
			inst.connect("COM3", 115200, 0, 8, 1);
			
			while(true) {
				logger.info("val: " + inst.sendRequest(1, RegisterRange.INPUT_REGISTER, 0x330A, DataType.TWO_BYTE_INT_UNSIGNED));
				Thread.sleep(2000);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        logger.info("done..:");
    }
    
    
    private SerialPort serialLocalPort;
	private ModbusMaster master;  
	
	public void connect(String serialPortName, int baud, int parity, int dataBits, int stopBits) throws Exception {
		ModbusFactory factory = new ModbusFactory();  

//		SerialPort[] ports = SerialPort.getCommPorts();
//		for (SerialPort serialPort : ports) {
//			System.out.println(serialPort.getSystemPortName());
//		}

		serialLocalPort= SerialPort.getCommPort(serialPortName);
		serialLocalPort.setBaudRate(baud);
		serialLocalPort.setParity(parity);
		serialLocalPort.setNumDataBits(dataBits);
		serialLocalPort.setNumStopBits(stopBits);

		SerialPortWrapper spw = new SerialPortWrapper()
		{
			public void open() throws Exception
			{
				// TODO Auto-generated method stub
				if (serialLocalPort.openPort())
				{
					
				}
			}

			public int getStopBits()
			{
				return serialLocalPort.getNumStopBits();
			}

			public int getParity()
			{
				return serialLocalPort.getParity();
			}

			public OutputStream getOutputStream()
			{
				return serialLocalPort.getOutputStream();
			}

			public InputStream getInputStream()
			{
				return serialLocalPort.getInputStream();
			}

			public int getFlowControlOut()
			{
				return 0;
			}

			public int getFlowControlIn()
			{
				return 0;
			}

			public int getDataBits()
			{
				return serialLocalPort.getNumDataBits();
			}

			public int getBaudRate()
			{
				return serialLocalPort.getBaudRate();
			}

			public void close() throws Exception
			{
				if (serialLocalPort.closePort())
				{
					
				}
			}
		};



	    master = factory.createRtuMaster(spw);  
	
	    master.setTimeout(5000);  
	    master.setRetries(0);  
	
	    try {
	        master.init();
	    } catch (Exception e) {
	        logger.error( "Modbus Master Init Error: " + e.getMessage());  
	        throw e;
	    }
	}
	
	
	public void disconnect() {
		master.destroy();  
	}
	
	
	public Number sendRequest(int modbusAddress, int functionCode, int register, int dataType) throws Exception {
//		logger.info("request: " + modbusAddress + "\t" + register);
		int iRetry = 0;
		
		while (iRetry < 10) {
			try {
				switch (functionCode) {
				case RegisterRange.HOLDING_REGISTER:
					BaseLocator<Number> loc = BaseLocator.holdingRegister(modbusAddress, register, dataType);
					return master.getValue(loc);
				case RegisterRange.INPUT_REGISTER:
					BaseLocator<Number> loc_input = BaseLocator.inputRegister(modbusAddress, register, dataType);
					return master.getValue(loc_input);
		
				default:
					throw new Exception("Invalid Function Code!");
				}
			} catch (Exception e) {
//				logger.error(modbusAddress + "\t" + functionCode + "\t" + register + "\t" + e.getMessage());
//				e.printStackTrace();
			}
		}
		return null;
	}


	public ModbusMaster getMaster() {
		return master;
	}
	
	
}
