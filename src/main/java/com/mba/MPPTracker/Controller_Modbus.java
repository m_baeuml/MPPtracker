package com.mba.MPPTracker;

import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mba.MPPTracker.Structs.StMPPTControl;
import com.mba.MPPTracker.Structs.StMPPTracker;
import com.mba.MPPTracker.Structs.StMPPTrackerList;
import com.mba.MPPTracker.Structs.StStatistical;
import com.mba.MPPTracker.Structs.StStatisticalList;
import com.serotonin.modbus4j.locator.BaseLocator;

public class Controller_Modbus extends Thread {
	private static Logger logger = LoggerFactory.getLogger(Controller_Modbus.class);
	
	private StMPPTrackerList trackerList;
	private StStatisticalList statisticList;
	private StMPPTControl control;
	
	private ModbusRTU modbusRTU;
	
	PausableExecutor executor = new PausableExecutor(1);
	public Controller_Modbus(StMPPTrackerList trackerList, StStatisticalList statisticList, StMPPTControl control) throws Exception {
		this.trackerList = trackerList;
		this.statisticList = statisticList;
		this.control = control;
		
		try {
			modbusRTU = new ModbusRTU();
			modbusRTU.connect("/dev/ttyUSB0", 115200, 0, 8, 1);
		} catch (Exception e) {
			e.printStackTrace();
//			throw e;
		}
	}

	public void run() {
		while(true) {
			try {
				try {
					executor.pause();
				} catch (Exception e) {
					
				}
				logger.info("start update...");
				for(StMPPTracker newTracker : trackerList) {
					newTracker.update(modbusRTU);
//					newTracker.debug();
				}
				
				for(StStatistical newStatistic : statisticList) {
					newStatistic.update(modbusRTU);
//					newStatistic.debug();
				}
				
												
				logger.info("executor resume...");
				try {
					executor.resume();
				} catch (Exception e) {
					
				}
				Thread.sleep(10*1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	
	public ScheduledFuture<Boolean> addCallableWrite(int modbusAddress, int register, boolean value) {
		Callable<Boolean> c = () -> {   // Lambda Expression
			BaseLocator<Boolean> loc = BaseLocator.coilStatus(modbusAddress, register);
			if(modbusRTU != null) {
				modbusRTU.getMaster().setValue(loc, value);
			}
			return true;
	    };
	    return executor.schedule(c, 0, TimeUnit.SECONDS);
	}
	
	public Callable<Number> addCallableInput(int modbusAddress, int register, int dataType) {
		Callable<Number> c = () -> {   // Lambda Expression
			BaseLocator<Number> loc = BaseLocator.inputRegister(modbusAddress, register, dataType);
			return modbusRTU.getMaster().getValue(loc);
	    };
	    return c;
	}
	
}
