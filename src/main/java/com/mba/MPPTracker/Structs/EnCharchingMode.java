package com.mba.MPPTracker.Structs;

public enum EnCharchingMode {
	DEFAULT(0),
	PWM(1),
	MPPT(2);
	
	
	private int i;
	
	EnCharchingMode(int i){
		this.i = i;
	}
	public int getI() {
		return i;
	}
	
	public static EnCharchingMode get(int i) {
		for(EnCharchingMode newVal : values()) {
			if(newVal.equals(i)) {
				return newVal;
			}
		}
		return DEFAULT;
	}
}
