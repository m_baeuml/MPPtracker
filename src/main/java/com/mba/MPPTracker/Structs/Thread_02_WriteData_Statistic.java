package com.mba.MPPTracker.Structs;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mba.MPPTracker.App;
import com.mba.MPPTracker.Controller_MySQL;


public class Thread_02_WriteData_Statistic implements Job  {
	private static Logger logger = LoggerFactory.getLogger(Thread_02_WriteData_Statistic.class);
	
	public void execute(JobExecutionContext context) throws JobExecutionException {
		logger.info("starting...");
		try {
			Controller_MySQL mysql = new Controller_MySQL();
			mysql.addStatistic(App.getStatisticList());
			
		} catch (Exception e) {
			logger.error(e.getClass().getName() + " " + e.getMessage());
			e.printStackTrace();
		}
		logger.info("done");
	}
}
