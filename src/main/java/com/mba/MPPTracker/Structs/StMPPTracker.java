package com.mba.MPPTracker.Structs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mba.MPPTracker.ModbusRTU;
import com.serotonin.modbus4j.code.DataType;
import com.serotonin.modbus4j.code.RegisterRange;

public class StMPPTracker {
	private static Logger logger = LoggerFactory.getLogger(StMPPTracker.class);
	private int id							= 0;
	private String name						= "";
	private int modbusAdress				= 1;
	
	private double pv_voltage				= 0;
	private double pv_current				= 0;
	private double pv_power					= 0;
	
	private double battery_voltage			= 0;
	private double battery_current			= 0;
	private double battery_power			= 0;
	private int	   battery_soc				= 0;
	private EnCharchingMode	charchingMode 	= EnCharchingMode.DEFAULT;
	
	private double load_voltage				= 0;
	private double load_current				= 0;
	private double load_power				= 0;
	
	private String createDate				= null;
	
	public StMPPTracker(int modbusAdress) {
		this.modbusAdress = modbusAdress;
	}


	public void update(ModbusRTU modbusRTU) throws Exception {
		pv_voltage = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 0x3100, DataType.TWO_BYTE_INT_UNSIGNED).doubleValue() / 100;
		pv_current = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 0x3101, DataType.TWO_BYTE_INT_UNSIGNED).doubleValue() / 100;
		pv_power   = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 0x3102, DataType.FOUR_BYTE_INT_UNSIGNED_SWAPPED).longValue();
		pv_power = pv_power / 100;
		
		battery_voltage  = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 0x331A, DataType.TWO_BYTE_INT_UNSIGNED).doubleValue() / 100;
		battery_current  = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 0x331B, DataType.FOUR_BYTE_INT_SIGNED_SWAPPED).longValue();
		battery_current = battery_current / 100;
		battery_power    = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 0x3106, DataType.FOUR_BYTE_INT_UNSIGNED_SWAPPED).longValue();
		battery_power = battery_power / 100;
		charchingMode    = EnCharchingMode.get(modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 0x3008, DataType.TWO_BYTE_INT_UNSIGNED).intValue());
		
		battery_soc  = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 0x311A, DataType.TWO_BYTE_INT_UNSIGNED).intValue();
		
		load_voltage = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 0X310C, DataType.TWO_BYTE_INT_UNSIGNED).doubleValue() / 100;
		load_current = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 0X310D, DataType.TWO_BYTE_INT_UNSIGNED).doubleValue() / 100;				
		load_power   = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 0X310E, DataType.FOUR_BYTE_INT_UNSIGNED_SWAPPED).floatValue();
		load_power = load_power / 100;
	}
	
	
	public void debug() {
		logger.info(modbusAdress + "\t" + "pv" + "\t" + "V" + "\t" + pv_voltage);
		logger.info(modbusAdress + "\t" + "pv" + "\t" + "A" + "\t" + pv_current);
		logger.info(modbusAdress + "\t" + "pv" + "\t" + "W" + "\t" + pv_power);
		logger.info("");
		logger.info(modbusAdress + "\t" + "battery" + "\t" + "V" + "\t" + battery_voltage);
		logger.info(modbusAdress + "\t" + "battery" + "\t" + "A" + "\t" + battery_current);
		logger.info(modbusAdress + "\t" + "battery" + "\t" + "W" + "\t" + battery_power);
		logger.info(modbusAdress + "\t" + "battery" + "\t" + "c" + "\t" + charchingMode);
		logger.info(modbusAdress + "\t" + "battery" + "\t" + "s" + "\t" + battery_soc);
		logger.info("");
		logger.info(modbusAdress + "\t" + "load" + "\t" + "V" + "\t" + load_voltage);
		logger.info(modbusAdress + "\t" + "load" + "\t" + "A" + "\t" + load_current);
		logger.info(modbusAdress + "\t" + "load" + "\t" + "W" + "\t" + load_power);
		logger.info("");
	}

	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public int getModbusAdress() {
		return modbusAdress;
	}
	public double getPv_voltage() {
		return pv_voltage;
	}
	public double getPv_current() {
		return pv_current;
	}
	public double getPv_power() {
		return pv_power;
	}
	public double getBattery_voltage() {
		return battery_voltage;
	}
	public double getBattery_current() {
		return battery_current;
	}
	public double getBattery_power() {
		return battery_power;
	}
	public int getBattery_soc() {
		return battery_soc;
	}
	public EnCharchingMode getCharchingMode() {
		return charchingMode;
	}
	public double getLoad_voltage() {
		return load_voltage;
	}
	public double getLoad_current() {
		return load_current;
	}
	public double getLoad_power() {
		return load_power;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setModbusAdress(int modbusAdress) {
		this.modbusAdress = modbusAdress;
	}
	public void setPv_voltage(double pv_voltage) {
		this.pv_voltage = pv_voltage;
	}
	public void setPv_current(double pv_current) {
		this.pv_current = pv_current;
	}
	public void setPv_power(double pv_power) {
		this.pv_power = pv_power;
	}
	public void setBattery_voltage(double battery_voltage) {
		this.battery_voltage = battery_voltage;
	}
	public void setBattery_current(double battery_current) {
		this.battery_current = battery_current;
	}
	public void setBattery_power(double battery_power) {
		this.battery_power = battery_power;
	}
	public void setBattery_soc(int battery_soc) {
		this.battery_soc = battery_soc;
	}
	public void setCharchingMode(EnCharchingMode charchingMode) {
		this.charchingMode = charchingMode;
	}
	public void setLoad_voltage(double load_voltage) {
		this.load_voltage = load_voltage;
	}
	public void setLoad_current(double load_current) {
		this.load_current = load_current;
	}
	public void setLoad_power(double load_power) {
		this.load_power = load_power;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getCreateDate() {
		return createDate;
	}

}

