package com.mba.MPPTracker.Structs;

import java.util.LinkedList;

public class StMPPTrackerList extends LinkedList<StMPPTracker>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2713974326361504361L;
	
	public static void main(String[] args) {
		StMPPTrackerList inst = new StMPPTrackerList();
		inst.add(new StMPPTracker(1));
		
		for(StMPPTracker newVal : inst) {
			newVal.debug();
		}
		
		StStatistical stat = new StStatistical(1);
		stat.debug();
	}

	public StMPPTracker getAddr(int i) {
		for(StMPPTracker newVal : this) {
			if(newVal.getModbusAdress() == i) {
				return newVal;
			}
		}
		return null;
	}

}
