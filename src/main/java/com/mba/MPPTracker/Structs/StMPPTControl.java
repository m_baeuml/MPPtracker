package com.mba.MPPTracker.Structs;

import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mba.MPPTracker.App;
import com.pi4j.io.gpio.GpioPinDigitalOutput;

public class StMPPTControl {
	private static Logger logger = LoggerFactory.getLogger(StMPPTControl.class);
		private GpioPinDigitalOutput power_gpio = null;
	
	private boolean autoSwitch = false;
	
	public void setLoadOn(int id) throws InterruptedException, ExecutionException{
		logger.info("set Load on: " + id);
		App.getModbus().addCallableWrite(id, 3, true).get();
		App.getModbus().addCallableWrite(id, 2, true).get();
	}
	
	public void setLoadOff(int id) throws InterruptedException, ExecutionException{
		logger.info("set Load off: " + id);
		App.getModbus().addCallableWrite(id, 3, true).get();
		App.getModbus().addCallableWrite(id, 2, false).get();
	}
	
	public void togglePinPower() throws InterruptedException {
		if(power_gpio != null) {
			power_gpio.high();
			Thread.sleep(1000);
			power_gpio.low();
			Thread.sleep(1000);
			power_gpio.high();
		} else {
			logger.warn("power_gpio is null!");
		}
	}

	public boolean isAutoSwitch() {
		return autoSwitch;
	}
	public void setAutoSwitch(boolean autoSwitch) {
		this.autoSwitch = autoSwitch;
	}
	public GpioPinDigitalOutput getPower_gpio() {
		return power_gpio;
	}
	public void setPower_gpio(GpioPinDigitalOutput power_gpio) {
		this.power_gpio = power_gpio;
	}
}
