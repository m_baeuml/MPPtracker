package com.mba.MPPTracker.Structs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mba.MPPTracker.ModbusRTU;
import com.serotonin.modbus4j.code.DataType;
import com.serotonin.modbus4j.code.RegisterRange;

public class StStatistical {
	private static Logger logger = LoggerFactory.getLogger(StStatistical.class);
	
	private int	modbusAdress 			= 1;
	
	private double max_pv_voltage				= 0;
	private double max_battery_voltage			= 0;
	private double min_battery_voltage			= 0;
	
	private double consumed_energy_day			= 0;
	private double consumed_energy_month		= 0;
	private double consumed_energy_year			= 0;
	private double consumed_energy_total		= 0;
	
	private double generated_energy_day			= 0;
	private double generated_energy_month		= 0;
	private double generated_energy_year		= 0;
	private double generated_energy_total		= 0;
	
	
	public StStatistical(int modbusAdress) {
		this.modbusAdress = modbusAdress;
	}


	public void update(ModbusRTU modbusRTU) throws Exception {
		if(modbusRTU != null) {
		max_pv_voltage = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 0x3001, DataType.TWO_BYTE_INT_UNSIGNED).intValue();
		max_pv_voltage = max_pv_voltage/ 100;
		
		max_battery_voltage = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 0x3302, DataType.TWO_BYTE_INT_UNSIGNED).intValue();
		max_battery_voltage = max_battery_voltage / 100;
		
		min_battery_voltage = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 0x3303, DataType.TWO_BYTE_INT_UNSIGNED).intValue();
		min_battery_voltage = min_battery_voltage / 100;
		
		
		consumed_energy_day = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 0x3304, DataType.FOUR_BYTE_INT_UNSIGNED_SWAPPED).longValue();
		consumed_energy_day = consumed_energy_day / 100;
		
		consumed_energy_month = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 0x3306, DataType.FOUR_BYTE_INT_UNSIGNED_SWAPPED).longValue();
		consumed_energy_month = consumed_energy_month / 100;
		
		consumed_energy_year = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 0x3308, DataType.FOUR_BYTE_INT_UNSIGNED_SWAPPED).longValue();
		consumed_energy_year = consumed_energy_year / 100;
		
		consumed_energy_total = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 0x330A, DataType.FOUR_BYTE_INT_UNSIGNED_SWAPPED).longValue();
		consumed_energy_total = consumed_energy_total / 100;
		
		
		
		
		generated_energy_day = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 0x330C, DataType.FOUR_BYTE_INT_UNSIGNED_SWAPPED).longValue();
		generated_energy_day = generated_energy_day / 100;
		
		generated_energy_month = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 0x330E, DataType.FOUR_BYTE_INT_UNSIGNED_SWAPPED).longValue();
		generated_energy_month = generated_energy_month / 100;
		
		generated_energy_year = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 0x3310, DataType.FOUR_BYTE_INT_UNSIGNED_SWAPPED).longValue();
		generated_energy_year = generated_energy_year / 100;
		
		generated_energy_total = modbusRTU.sendRequest(modbusAdress, RegisterRange.INPUT_REGISTER, 0x3312, DataType.FOUR_BYTE_INT_UNSIGNED_SWAPPED).longValue();
		generated_energy_total = generated_energy_total / 100;
		}
	}
	
	public void debug() {
		logger.info(modbusAdress + "\t" + "max_pv_voltage" + "\t" + "V" + "\t" + max_pv_voltage);
		logger.info(modbusAdress + "\t" + "max_battery_voltage" + "\t" + "A" + "\t" + max_battery_voltage);
		logger.info(modbusAdress + "\t" + "min_battery_voltage" + "\t" + "W" + "\t" + min_battery_voltage);
		logger.info("");
		logger.info(modbusAdress + "\t" + "consumed" + "\t" + "day" 	+ "\t" + consumed_energy_day);
		logger.info(modbusAdress + "\t" + "consumed" + "\t" + "month" 	+ "\t" + consumed_energy_month);
		logger.info(modbusAdress + "\t" + "consumed" + "\t" + "year" 	+ "\t" + consumed_energy_year);
		logger.info(modbusAdress + "\t" + "consumed" + "\t" + "total" 	+ "\t" + consumed_energy_total);
		logger.info("");
		logger.info(modbusAdress + "\t" + "generated" + "\t" + "day" 	+ "\t" + generated_energy_day);
		logger.info(modbusAdress + "\t" + "generated" + "\t" + "month" 	+ "\t" + generated_energy_month);
		logger.info(modbusAdress + "\t" + "generated" + "\t" + "year" 	+ "\t" + generated_energy_year);
		logger.info(modbusAdress + "\t" + "generated" + "\t" + "total" 	+ "\t" + generated_energy_total);
		logger.info("");
	}


	public int getModbusAdress() {
		return modbusAdress;
	}
	public double getMax_pv_voltage() {
		return max_pv_voltage;
	}
	public double getMax_battery_voltage() {
		return max_battery_voltage;
	}
	public double getMin_battery_voltage() {
		return min_battery_voltage;
	}
	public double getConsumed_energy_day() {
		return consumed_energy_day;
	}
	public double getConsumed_energy_month() {
		return consumed_energy_month;
	}
	public double getConsumed_energy_year() {
		return consumed_energy_year;
	}
	public double getConsumed_energy_total() {
		return consumed_energy_total;
	}
	public double getGenerated_energy_day() {
		return generated_energy_day;
	}
	public double getGenerated_energy_month() {
		return generated_energy_month;
	}
	public double getGenerated_energy_year() {
		return generated_energy_year;
	}
	public double getGenerated_energy_total() {
		return generated_energy_total;
	}
}
