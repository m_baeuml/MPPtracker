package com.mba.MPPTracker.Structs;

public enum EnBatteryStatus {
	NORMAL(0),
	OVERVOLT(1),
	UNDERVOLT(2),
	LOWVOLT(3),
	FAULT(4);
	
	private int i;
	
	EnBatteryStatus(int i){
		this.i = i;
	}
	public int getI() {
		return i;
	}
}
