package com.mba.MPPTracker;

import java.util.LinkedList;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mba.MPPTracker.Structs.StMPPTracker;
import com.mba.MPPTracker.Structs.StMPPTrackerList;
import com.mba.MPPTracker.Structs.Web.StButtonResponse;

@Controller
public class WebInterface {
	
	@ResponseBody
	@RequestMapping(value = "/api_pv/loadOn/{id}", method = RequestMethod.GET)
	public StButtonResponse loadOn(
			@PathVariable int id
			){
		StButtonResponse response = new StButtonResponse();
		try {
			App.getControl().setLoadOn(id);
			response.setStatus(1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@ResponseBody
	@RequestMapping(value = "/api_pv/loadOff/{id}", method = RequestMethod.GET)
	public StButtonResponse loadOff(
			@PathVariable int id
			)
	{
		StButtonResponse response = new StButtonResponse();
		try {
			App.getControl().setLoadOff(id);
			response.setStatus(1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/api_pv/togglePowerPin", method = RequestMethod.GET)
	public StButtonResponse togglePinPower()
	{
		StButtonResponse response = new StButtonResponse();
		try {
			App.getControl().togglePinPower();
			response.setStatus(1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	@ResponseBody
	@RequestMapping(value = "/api_pv/switchAuto/On", method = RequestMethod.GET)
	public StButtonResponse api_autoSwitch_On()
	{
		StButtonResponse response = new StButtonResponse();
		try {
			App.getControl().setAutoSwitch(true);
			response.setStatus(1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@ResponseBody
	@RequestMapping(value = "/api_pv/switchAuto/Off", method = RequestMethod.GET)
	public StButtonResponse api_autoSwitch_Off()
	{
		StButtonResponse response = new StButtonResponse();
		try {
			App.getControl().setAutoSwitch(false);
			response.setStatus(1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	
	
	
	
	
	
	@ResponseBody
	@RequestMapping(value = "/api_pv/pv/tracker", method = RequestMethod.GET)
	public StMPPTrackerList pv_tracker()
	{
		 return App.getTrackerList();
	}
	
	@ResponseBody
	@RequestMapping(value = "/api_pv/pv/tracker/hist", method = RequestMethod.GET)
	public LinkedList<StMPPTracker> pv_tracker_hist()
	{
		 return new Controller_MySQL().getLastValues();
	}
	
	@ResponseBody
	@RequestMapping(value = "/api_pv/pv/statistic", method = RequestMethod.GET)
	public StMPPTrackerList pv_statistic()
	{
		 return App.getTrackerList();
	}
}
