package com.mba.MPPTracker;

import java.util.Date;
import java.util.GregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mba.MPPTracker.Structs.StMPPTControl;
import com.mba.MPPTracker.Structs.StMPPTrackerList;

public class Controller_LoadIO  extends Thread {
	private static Logger logger = LoggerFactory.getLogger(Controller_LoadIO.class);
	private StMPPTrackerList trackerList;
	
	public Controller_LoadIO(StMPPTrackerList trackerList, StMPPTControl control) {
		this.trackerList = trackerList;
	}
	
	int iCounterOn = 0;
	public void run() {
		StMPPTControl control = App.getControl();
		GregorianCalendar now;
		while(true) {
			now = new GregorianCalendar();
			now.setTime(new Date());
			try {
				if(control != null) {
					if(control.isAutoSwitch()) {
						logger.info("check farm...");
						//Ausschalten
						if(trackerList.getAddr(2).getLoad_power() + trackerList.getAddr(3).getLoad_power() > 4) {
							logger.info("check off " + trackerList.getAddr(2).getBattery_soc() + "\t" + trackerList.getAddr(3).getBattery_soc());
							if(trackerList.getAddr(2).getBattery_soc() < 23
							|| trackerList.getAddr(3).getBattery_soc() < 23) {
								control.togglePinPower();
								logger.info("wait power off");
								Thread.sleep(120*1000);
								control.setLoadOff(2);
								control.setLoadOff(3);
								logger.info("wait power off");
								for (int i = 0; i < 20; i++) {
									logger.info("sleep power off " + i);
									Thread.sleep(60*1000);
								}
								
							}
						} else {
							double pvPower = trackerList.getAddr(2).getPv_power() + trackerList.getAddr(1).getPv_power() + trackerList.getAddr(3).getPv_power();
							logger.info("check on " + trackerList.getAddr(2).getBattery_soc() + "\t" + trackerList.getAddr(3).getBattery_soc() + "\t" + pvPower);
							//Einschalten
							
							if((trackerList.getAddr(2).getBattery_soc() > 92
							&&  trackerList.getAddr(3).getBattery_soc() > 92
							&&  now.get(GregorianCalendar.HOUR_OF_DAY) >= 8)) {
								if(iCounterOn > 10 || pvPower > 130) {
									iCounterOn = 0;
								
									control.setLoadOn(2);
									control.setLoadOn(3);
									logger.info("wait load on...");
									Thread.sleep(10*1000);
									
									control.togglePinPower();
									logger.info("wait power on...");
									Thread.sleep(120*1000);
									logger.info("power on done!");
								} else {
									iCounterOn = iCounterOn + 1;
									logger.info("start counter... " + iCounterOn);
									Thread.sleep(60*1000);
								}
							}
							
							if(trackerList.getAddr(2).getBattery_soc() < 75) {
								iCounterOn = 0;
							}
						}
					}
				} else {
					logger.error("control is null!");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try {
				Thread.sleep(30*1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
