package com.mba.MPPTracker;

import com.mba.MPPTracker.Structs.StMPPTrackerList;
import com.mba.MPPTracker.Structs.StMPPTControl;
import com.mba.MPPTracker.Structs.StMPPTracker;
import com.mba.MPPTracker.Structs.StStatistical;
import com.mba.MPPTracker.Structs.StStatisticalList;
import com.mba.MPPTracker.Structs.Thread_01_WriteData;
//import com.pi4j.io.gpio.GpioController;
//import com.pi4j.io.gpio.GpioFactory;
//import com.pi4j.io.gpio.GpioPinDigitalOutput;
//import com.pi4j.io.gpio.PinState;
//import com.pi4j.io.gpio.RaspiPin;
import com.mba.MPPTracker.Structs.Thread_02_WriteData_Statistic;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import com.pi4j.util.CommandArgumentParser;
import com.pi4j.util.Console;
import com.pi4j.util.ConsoleColor;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

@SpringBootApplication
public class App {
	private static Logger logger = LoggerFactory.getLogger(App.class);
	
	private static StMPPTrackerList trackerList = new StMPPTrackerList();
	private static StStatisticalList statisticList = new StStatisticalList();
	private static StMPPTControl control = new StMPPTControl();
	
	private static Controller_Modbus modbus = null;
	private static Controller_LoadIO loadIo = null;
	
	private static final Console console = new Console();
	private static final GpioController gpio = GpioFactory.getInstance();
	
	public static void main(String[] args) {
		System.setProperty("spring.devtools.restart.enabled", "false");
		
		
		SpringApplication.run(App.class, args);
	}


	@EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) {
		logger.info("START!!");
		SchedulerFactory sf = new StdSchedulerFactory();
		
		Scheduler sched = null;
		try {
			sched = sf.getScheduler();
			
			JobDetail job_tracker = JobBuilder.newJob(Thread_01_WriteData.class)
				    .withIdentity("job_tracker_write_all_data", "15min")
				    .build();
			
			Trigger trigger_15min = TriggerBuilder.newTrigger()
				    .withIdentity("crontrigger", "15min")
				    .withSchedule(CronScheduleBuilder.cronSchedule("0 0/5 * ? * * *"))
				    .build();
			
			sched.scheduleJob(job_tracker, trigger_15min);
			
			
			
			JobDetail job_statistic = JobBuilder.newJob(Thread_02_WriteData_Statistic.class)
				    .withIdentity("mysql_statistic_write_all_data", "15min")
				    .build();
			
			Trigger trigger_statistic = TriggerBuilder.newTrigger()
				    .withIdentity("mysql_statistic_crontrigger", "mysql_statistic")
//				    .withSchedule(CronScheduleBuilder.cronSchedule("0 30 23 ? * * *"))
				    .withSchedule(CronScheduleBuilder.cronSchedule("0 0 0/1 ? * * *"))
				    .build();
			sched.scheduleJob(job_statistic, trigger_statistic);
			
			sched.start();
			
		} catch (SchedulerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		
		
		
		GpioPinDigitalOutput power_gpio = null;
		try {
			String[] args = "".split("");
			Pin power_pin = CommandArgumentParser.getPin(
			            RaspiPin.class,    // pin provider class to obtain pin instance from
			            RaspiPin.GPIO_00,  // default pin if no pin argument found
			            args);             // argument array to search in
			power_gpio = gpio.provisionDigitalOutputPin(power_pin, "Power", PinState.HIGH);
			power_gpio.setShutdownOptions(false, PinState.HIGH);
			
			
			// create a pin listener to print out changes to the output gpio pin state
			power_gpio.addListener(new GpioPinListenerDigital() {
	            @Override
	            public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
	                // display pin state on console
	                console.println(" --> GPIO PIN STATE CHANGE: " + event.getPin() + " = " +
	                        ConsoleColor.conditional(
	                                event.getState().isHigh(), // conditional expression
	                                ConsoleColor.GREEN,        // positive conditional color
	                                ConsoleColor.RED,          // negative conditional color
	                                event.getState()));        // text to display
	            }
	        });
	        
	        
			control.setPower_gpio(power_gpio);
		} catch (Exception e) {
			logger.warn("gpio init power failed!");
		}
		
		trackerList.add(new StMPPTracker(1));
		trackerList.add(new StMPPTracker(2));
		trackerList.add(new StMPPTracker(3));
		
		statisticList.add(new StStatistical(1));
		statisticList.add(new StStatistical(2));
		statisticList.add(new StStatistical(3));
		try {
			modbus = new Controller_Modbus(trackerList, statisticList, control);
			modbus.start();	
			
			loadIo = new Controller_LoadIO(trackerList, control);
			loadIo.start();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {

//			System.out.println("Let's inspect the beans provided by Spring Boot:");
//			String[] beanNames = ctx.getBeanDefinitionNames();
//			Arrays.sort(beanNames);
//			for (String beanName : beanNames) {
//				System.out.println(beanName);
//			}

		};
	}


	public static StMPPTControl getControl() {
		return control;
	}
	public static StMPPTrackerList getTrackerList() {
		return trackerList;
	}
	public static StStatisticalList getStatisticList() {
		return statisticList;
	}


	public static Controller_Modbus getModbus() {
		return modbus;
	}


	public static Controller_LoadIO getLoadIo() {
		return loadIo;
	}
}
