package com.mba.MPPTracker;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mba.MPPTracker.Structs.StMPPTracker;
import com.mba.MPPTracker.Structs.StMPPTrackerList;
import com.mba.MPPTracker.Structs.StStatistical;
import com.mba.MPPTracker.Structs.StStatisticalList;
import com.mba.Tasmota.StSensor;
import com.mba.fritzBox.StFritzboxDataTraffic;
import com.mba.inverter.Structs.StInverterList;
import com.mba.inverter.Structs.StInverterStatistic;

//GRANT ALL PRIVILEGES ON *.* TO 'root'@'192.168.100.%' IDENTIFIED BY 'my-new-password' WITH GRANT OPTION;

public class Controller_MySQL {
	private static Logger logger = LoggerFactory.getLogger(StMPPTracker.class);
	
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public void addValue(StMPPTrackerList list) {
		GregorianCalendar newDate = new GregorianCalendar();
		newDate.set(GregorianCalendar.SECOND, 0);
		
		Connection con = null;
		try {
			con = getConnection();
			PreparedStatement stmt = con.prepareStatement(
					"INSERT INTO pv_measure.tbl_measurement\r\n"
					+ "(pv_voltage, pv_current, pv_power, battery_voltage, battery_current, battery_power, battery_soc, charchingMode, load_voltage, load_current, load_power, createdate, modbus_address)\r\n"
					+ "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);\r\n"
					+ "");
			
			for(StMPPTracker newVal : list) {
				stmt.setDouble(1, newVal.getPv_voltage());
				stmt.setDouble(2, newVal.getPv_current());
				stmt.setDouble(3, newVal.getPv_power());
				
				stmt.setDouble(4, newVal.getBattery_voltage());
				stmt.setDouble(5, newVal.getBattery_current());
				stmt.setDouble(6, newVal.getBattery_power());
				stmt.setInt(7, newVal.getBattery_soc());
				
				stmt.setInt(8, newVal.getCharchingMode().getI());
				
				stmt.setDouble(9, newVal.getLoad_voltage());
				stmt.setDouble(10, newVal.getLoad_current());
				stmt.setDouble(11, newVal.getLoad_power());
				
				stmt.setString(12, sdf.format(newDate.getTime()));
				stmt.setInt(13, newVal.getModbusAdress());
				stmt.addBatch();
			}
			
			logger.info("added " + stmt.executeBatch().length + " tracker values..");
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {con.close();} catch (Exception e1) {};
		}
	}
	
	
	public LinkedList<StMPPTracker> getLastValues() {
		LinkedList<StMPPTracker> returnData = new LinkedList<StMPPTracker>();
		GregorianCalendar newDate = new GregorianCalendar();
		newDate.add(GregorianCalendar.HOUR_OF_DAY, -24);
		
		Connection con = null;
		try {
			String query =  
					"SELECT measurement_id, pv_voltage, pv_current, pv_power, battery_voltage, battery_current, battery_power, battery_soc, charchingMode, load_voltage, load_current, load_power, createdate, modbus_address\r\n"
					+ "FROM pv_measure.tbl_measurement\r\n"
					+ "WHERE createdate > '" + sdf.format(newDate.getTime()) + "';";
			logger.info(query);
			con = getConnection();
			ResultSet rs = con.createStatement().executeQuery(query);
			
			
			while(rs.next()) {
				StMPPTracker newVal = new StMPPTracker(rs.getInt("modbus_address"));
				newVal.setPv_voltage(rs.getDouble("pv_voltage"));
				newVal.setPv_current(rs.getDouble("pv_current"));
				newVal.setPv_power(rs.getDouble("pv_power"));
				newVal.setBattery_voltage(rs.getDouble("battery_voltage"));
				newVal.setBattery_current(rs.getDouble("battery_current"));
				newVal.setBattery_power(rs.getDouble("battery_power"));
				newVal.setBattery_soc(rs.getInt("battery_soc"));
				newVal.setLoad_voltage(rs.getDouble("load_voltage"));
				newVal.setLoad_current(rs.getDouble("load_current"));
				newVal.setLoad_power(rs.getDouble("load_power"));
				newVal.setCreateDate(rs.getString("createdate"));
				returnData.add(newVal);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {con.close();} catch (Exception e1) {};
		}
		return returnData;
	}
	
//	public static String connectionIp = "localhost";
	public static String connectionIp = "192.168.178.210";
	private Connection getConnection() throws Exception {
		return DriverManager.getConnection("jdbc:mysql://" + connectionIp + ":3306","pv","raspberry");  
	}
	
	
	
	
	public void addStatistic(StStatisticalList list) {
		GregorianCalendar newDate = new GregorianCalendar();
		newDate.set(GregorianCalendar.SECOND, 0);
		
		Connection con = null;
		try {
			con = getConnection();
			PreparedStatement stmt = con.prepareStatement(
					"INSERT INTO pv_measure.tbl_statistic\r\n"
					+ "(max_pv_voltage, max_battery_voltage, min_battery_voltage, consumed_energy_day, consumed_energy_month, consumed_energy_year, consumed_energy_total, generated_energy_day, generated_energy_month, generated_energy_year, generated_energy_total, createdate, modbus_address)\r\n"
					+ "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);\r\n"
					+ "");
			
			for(StStatistical newVal : list) {
				stmt.setDouble(1, newVal.getMax_pv_voltage());
				stmt.setDouble(2, newVal.getMax_battery_voltage());
				stmt.setDouble(3, newVal.getMin_battery_voltage());
				
				stmt.setDouble(4, newVal.getConsumed_energy_day());
				stmt.setDouble(5, newVal.getConsumed_energy_month());
				stmt.setDouble(6, newVal.getConsumed_energy_year());
				stmt.setDouble(7, newVal.getConsumed_energy_total());
				
				stmt.setDouble(8, newVal.getGenerated_energy_day());
				stmt.setDouble(9, newVal.getGenerated_energy_month());
				stmt.setDouble(10, newVal.getGenerated_energy_year());
				stmt.setDouble(11, newVal.getGenerated_energy_total());
				
				stmt.setString(12, sdf.format(newDate.getTime()));
				stmt.setInt(13, newVal.getModbusAdress());
				stmt.addBatch();
			}
			
			logger.info("added " + stmt.executeBatch().length + " statistic values..");
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {con.close();} catch (Exception e1) {};
		}
	}


	private void setInt(PreparedStatement stmt, int id, Integer value) throws SQLException {
		if(value == null) {
			stmt.setObject(id, value);
		} else {
			stmt.setInt(id, value);
		}
	}
	
	public void addValue(StInverterList inverterList) {
		if(inverterList.size() == 0) {
			return;
		}
		GregorianCalendar newDate = new GregorianCalendar();
		newDate.set(GregorianCalendar.SECOND, 0);
		
		Connection con = null;
		try {
			con = getConnection();
			PreparedStatement stmt = con.prepareStatement(
					"INSERT INTO pv_measure.tbl_inverter\r\n"
					+ "(pv_voltage_min, pv_voltage_avg, pv_voltage_max, "
					+ "pv_current_min, pv_current_avg, pv_current_max,"
					+ "pv_power_min,pv_power_avg,pv_power_max,"
					+ "ac_voltage_min,ac_voltage_avg,ac_voltage_max,"
					+ "ac_current_min,ac_current_avg,ac_current_max,"
					+ "energyToday, energyTotal, createdate, modbus_address, listcount)\r\n"
					+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);\r\n"
					+ "");
			
			int listSize = 0;
			synchronized (inverterList) {
				this.setInt(stmt, 1, inverterList.getMinVoltage());
				this.setInt(stmt, 2, inverterList.getAvgVoltage());
				this.setInt(stmt, 3, inverterList.getMaxVoltage());
				
				this.setInt(stmt, 4, inverterList.getMinCurrent());
				this.setInt(stmt, 5, inverterList.getAvgCurrent());
				this.setInt(stmt, 6, inverterList.getMaxCurrent());
	
				this.setInt(stmt, 7, inverterList.getMinPower());
				this.setInt(stmt, 8, inverterList.getAvgPower());
				this.setInt(stmt, 9, inverterList.getMaxPower());
				
				this.setInt(stmt, 10, inverterList.getMinVac1());
				this.setInt(stmt, 11, inverterList.getAvgVac1());
				this.setInt(stmt, 12, inverterList.getMaxVac1());
	
				this.setInt(stmt, 13, inverterList.getMinIac1());
				this.setInt(stmt, 14, inverterList.getAvgIac1());
				this.setInt(stmt, 15, inverterList.getMaxIac1());
			
				
				stmt.setInt(16, inverterList.getEnergy_today());
				stmt.setInt(17, inverterList.getEnergy_total());
				
				stmt.setString(18, sdf.format(newDate.getTime()));
				stmt.setInt(19, inverterList.getModbusAdress());
				stmt.setInt(20, inverterList.size());
				stmt.addBatch();
				
				listSize = inverterList.size();
				inverterList.clear();
			}
			
			logger.info("added " + stmt.executeBatch().length + " inverter values.. " + listSize);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {con.close();} catch (Exception e1) {};
		}
	}


	public LinkedList<StInverterStatistic> getInverterLastValues() {
		LinkedList<StInverterStatistic> returnData = new LinkedList<StInverterStatistic>();
		GregorianCalendar newDate = new GregorianCalendar();
		newDate.add(GregorianCalendar.HOUR_OF_DAY, -24);
		
		Connection con = null;
		try {
			String query =  
					"SELECT id, pv_voltage_min, pv_voltage_avg, pv_voltage_max, pv_current_min, pv_current_avg, pv_current_max, pv_power_min, pv_power_avg, pv_power_max, ac_voltage_min, ac_voltage_avg, ac_voltage_max, ac_current_min, ac_current_avg, ac_current_max, energyToday, energyTotal, createdate, modbus_address, listcount\r\n"
					+ "FROM pv_measure.tbl_inverter\r\n"
					+ "WHERE createdate > '" + sdf.format(newDate.getTime()) + "';";
			logger.info(query);
			con = getConnection();
			ResultSet rs = con.createStatement().executeQuery(query);
			
			
			while(rs.next()) {
				StInverterStatistic newVal = new StInverterStatistic(rs);
				returnData.add(newVal);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {con.close();} catch (Exception e1) {};
		}
		return returnData;
	}


	public void addValue(StSensor energySensor, String macAddr) {
		GregorianCalendar newDate = new GregorianCalendar();
		newDate.set(GregorianCalendar.SECOND, 0);
		
		Connection con = null;
		try {
			con = getConnection();
			PreparedStatement stmt = con.prepareStatement(
					"INSERT INTO pv_measure.tbl_energy\r\n"
					+ "(creationdate, energy_voltage, energy_current, energy_total, energy_yesterday, energy_today, apparentPower, reactivePower, power_factor, consumer, mac_addr, energy_period)\r\n"
					+ "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);\r\n"
					+ "");
			
			
			stmt.setString(1, sdf.format(energySensor.getTime()));
			stmt.setDouble(2, energySensor.getEnergy().getVoltage());
			stmt.setDouble(3, energySensor.getEnergy().getCurrent());
			
			stmt.setDouble(4, energySensor.getEnergy().getEnergy_total());
			stmt.setDouble(5, energySensor.getEnergy().getEnergy_yesterday());
			stmt.setDouble(6, energySensor.getEnergy().getEnergy_today());
			stmt.setDouble(7, energySensor.getEnergy().getApparentPower());
			stmt.setDouble(8, energySensor.getEnergy().getReactivePower());
			stmt.setDouble(9, energySensor.getEnergy().getPowerFactor());
			stmt.setString(10, energySensor.getConsumer());
			stmt.setString(11, macAddr);
			stmt.setDouble(12, energySensor.getEnergy().getEnergy_period());
			stmt.addBatch();
			
			
			logger.info("added " + stmt.executeBatch().length + " StEnergySensor values..");
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {con.close();} catch (Exception e1) {};
		}
	}
	
	
	
	
	
	public void addValue(StFritzboxDataTraffic value) {
		GregorianCalendar newDate = new GregorianCalendar();
		newDate.set(GregorianCalendar.SECOND, 0);
		
		Connection con = null;
		try {
			con = getConnection();
			PreparedStatement stmt = con.prepareStatement(
					"INSERT INTO pv_measure.tbl_fritzbox\r\n"
					+ "(createdate, externalIP, uptime, totalBytesSent, totalBytesReceived)\r\n"
					+ "VALUES(CURRENT_TIMESTAMP, ?, ?, ?, ?);\r\n"
					+ "");
			
			
			stmt.setString(1, value.getExternalIP());
			stmt.setLong(2, value.getUpTime());
			stmt.setLong(3, value.getNewTotalBytesSent());
			stmt.setLong(4, value.getNewTotalBytesReceived());
			stmt.addBatch();
			
			
			logger.info("added " + stmt.executeBatch().length + " StFritzboxDataTraffic values..");
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {con.close();} catch (Exception e1) {};
		}
	}
}
