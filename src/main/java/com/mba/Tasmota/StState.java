package com.mba.Tasmota;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StState {
	private static Logger logger = LoggerFactory.getLogger(StState.class);
/*

tele/tasmota_17C78A/STATE

{
  "Time": "2023-04-20T13:45:39",
  "Uptime": "0T00:05:09",
  "UptimeSec": 309,
  "Heap": 26,
  "SleepMode": "Dynamic",
  "Sleep": 50,
  "LoadAvg": 21,
  "MqttCount": 1,
  "POWER": "ON",
  "Wifi": {
    "AP": 1,
    "SSId": "FRITZ!Box 5Ghz!",
    "BSSId": "48:5D:35:10:F4:C3",
    "Channel": 1,
    "Mode": "11n",
    "RSSI": 100,
    "Signal": -44,
    "LinkCount": 1,
    "Downtime": "0T00:00:03"
  }
}


*/
	@JsonProperty("Time")
	private Date time;
	
	@JsonProperty("UptimeSec")
	private long uptimesec;
	
	@JsonProperty("LoadAvg")
	private int loadAvg;
	
	@JsonProperty("POWER")
	private boolean power;
	
	@JsonProperty("Wifi.Signal")
	private int wifiSignal;
	
	@JsonSetter("POWER")
	private void setPower(String power) {
		this.power = power.equals("ON");
	}
	
	
	
	public static void main(String[] args) {
		String testJson = "{\r\n"
				+ "  \"Time\": \"2023-04-20T13:45:39\",\r\n"
				+ "  \"Uptime\": \"0T00:05:09\",\r\n"
				+ "  \"UptimeSec\": 309,\r\n"
				+ "  \"Heap\": 26,\r\n"
				+ "  \"SleepMode\": \"Dynamic\",\r\n"
				+ "  \"Sleep\": 50,\r\n"
				+ "  \"LoadAvg\": 21,\r\n"
				+ "  \"MqttCount\": 1,\r\n"
				+ "  \"POWER\": \"ON\",\r\n"
				+ "  \"Wifi\": {\r\n"
				+ "    \"AP\": 1,\r\n"
				+ "    \"SSId\": \"FRITZ!Box 5Ghz!\",\r\n"
				+ "    \"BSSId\": \"48:5D:35:10:F4:C3\",\r\n"
				+ "    \"Channel\": 1,\r\n"
				+ "    \"Mode\": \"11n\",\r\n"
				+ "    \"RSSI\": 100,\r\n"
				+ "    \"Signal\": -44,\r\n"
				+ "    \"LinkCount\": 1,\r\n"
				+ "    \"Downtime\": \"0T00:00:03\"\r\n"
				+ "  }\r\n"
				+ "}";
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			StState obj = mapper.readValue(testJson, StState.class);
			logger.info("Time: " + "\t" + obj.time);
			logger.info("Uptim: " + "\t" + obj.uptimesec);
			logger.info("LoadAvg: " + "\t" + obj.loadAvg);
			logger.info("Power: " + "\t" + obj.power);
			logger.info("Signal: " + "\t" + obj.wifiSignal);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}




	