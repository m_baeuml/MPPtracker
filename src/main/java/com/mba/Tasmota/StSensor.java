package com.mba.Tasmota;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StSensor {
	private static Logger logger = LoggerFactory.getLogger(StSensor.class);
	
	@JsonProperty("Time")
	private Date time;
	
	@JsonProperty("ENERGY")
	private StEnergy energy;
	
	private String consumer;
	
	
	public static void main(String[] args) {
		String testJson = "{\r\n"
				+ "  \"Time\": \"2023-04-20T13:45:39\",\r\n"
				+ "  \"ENERGY\": {\r\n"
				+ "    \"TotalStartTime\": \"2023-04-13T17:42:36\",\r\n"
				+ "    \"Total\": 0.257,\r\n"
				+ "    \"Yesterday\": 0,\r\n"
				+ "    \"Today\": 0.257,\r\n"
				+ "    \"Period\": 0,\r\n"
				+ "    \"Power\": 0,\r\n"
				+ "    \"ApparentPower\": 0,\r\n"
				+ "    \"ReactivePower\": 0,\r\n"
				+ "    \"Factor\": 0,\r\n"
				+ "    \"Voltage\": 228,\r\n"
				+ "    \"Current\": 0\r\n"
				+ "  }\r\n"
				+ "}";
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			StSensor obj = mapper.readValue(testJson, StSensor.class);
			logger.info("Time: " + "\t" + obj.time);
			logger.info("Time: " + "\t" + obj.energy.getEnergy_today());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Date getTime() {
		return time;
	}

	public StEnergy getEnergy() {
		return energy;
	}
	public String getConsumer() {
		return consumer;
	}

	public void setConsumer(String consumer) {
		this.consumer = consumer;
	}
}
