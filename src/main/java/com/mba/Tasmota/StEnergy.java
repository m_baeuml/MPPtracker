package com.mba.Tasmota;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StEnergy {
	private static Logger logger = LoggerFactory.getLogger(StEnergy.class);
/*

tele/tasmota_17C78A/SENSOR

{
  "Time": "2023-04-20T13:45:39",
  "ENERGY": {
    "TotalStartTime": "2023-04-13T17:42:36",
    "Total": 0.257,
    "Yesterday": 0,
    "Today": 0.257,
    "Period": 0,
    "Power": 0,
    "ApparentPower": 0,
    "ReactivePower": 0,
    "Factor": 0,
    "Voltage": 228,
    "Current": 0
  }
}
	 
 */
	
	@JsonProperty("Total")
	private double energy_total;
	
	@JsonProperty("Yesterday")
	private double energy_yesterday;

	@JsonProperty("Today")
	private double energy_today;

	@JsonProperty("Period")
	private double energy_period;
	
	@JsonProperty("Power")
	private double power;
	
	@JsonProperty("ApparentPower")
	private double ApparentPower;
	
	@JsonProperty("ReactivePower")
	private double ReactivePower;
	
	@JsonProperty("Factor")
	private double powerFactor;
	
	@JsonProperty("Voltage")
	private double voltage;
	
	@JsonProperty("Current")
	private double current;


	public double getEnergy_total() {
		return energy_total;
	}

	public double getEnergy_yesterday() {
		return energy_yesterday;
	}

	public double getEnergy_today() {
		return energy_today;
	}

	public double getPower() {
		return power;
	}

	public double getApparentPower() {
		return ApparentPower;
	}

	public double getReactivePower() {
		return ReactivePower;
	}

	public double getPowerFactor() {
		return powerFactor;
	}

	public double getVoltage() {
		return voltage;
	}

	public double getCurrent() {
		return current;
	}
	public double getEnergy_period() {
		return energy_period;
	}
}
