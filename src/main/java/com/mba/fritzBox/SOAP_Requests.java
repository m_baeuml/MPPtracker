package com.mba.fritzBox;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

public class SOAP_Requests {
	
	/** GetOnlineMonitor **/
	
	/*
	 	<serviceType>urn:schemas-upnp-org:service:WANCommonInterfaceConfig:1</serviceType>
		<serviceId>urn:upnp-org:serviceId:WANCommonIFC1</serviceId>
		<controlURL>/igdupnp/control/WANCommonIFC1</controlURL>
		<eventSubURL>/igdupnp/control/WANCommonIFC1</eventSubURL>
		<SCPDURL>/igdicfgSCPD.xml</SCPDURL>
	 */
	public static SOAPMessage createSOAPRequest_GetOnlineMonitor(String Value) throws Exception {
	    MessageFactory messageFactory = MessageFactory.newInstance();
	    SOAPMessage soapMessage = messageFactory.createMessage();
	    SOAPPart soapPart = soapMessage.getSOAPPart();

	    SOAPEnvelope envelope = soapPart.getEnvelope();
	    envelope.addNamespaceDeclaration("u", "urn:schemas-upnp-org:service:WANCommonInterfaceConfig:1");

	    SOAPBody soapBody = envelope.getBody();
	    soapBody.addChildElement("Get" + Value, "u");

	    MimeHeaders headers = soapMessage.getMimeHeaders();
	    headers.addHeader("SOAPACTION", "urn:schemas-upnp-org:service:WANCommonInterfaceConfig:1#Get" + Value);

	    soapMessage.saveChanges();       	

	    return soapMessage;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/** GetExternalIP **/
	public static SOAPMessage createSOAPRequest_GetExternalIP() throws Exception {
	    MessageFactory messageFactory = MessageFactory.newInstance();
	    SOAPMessage soapMessage = messageFactory.createMessage();
	    SOAPPart soapPart = soapMessage.getSOAPPart();

	    SOAPEnvelope envelope = soapPart.getEnvelope();
	    envelope.addNamespaceDeclaration("u", "urn:schemas-upnp-org:service:WANIPConnection:1");

	    SOAPBody soapBody = envelope.getBody();
	    soapBody.addChildElement("GetExternalIPAddress", "u");

	    MimeHeaders headers = soapMessage.getMimeHeaders();
	    headers.addHeader("SOAPACTION", "urn:schemas-upnp-org:service:WANIPConnection:1#GetExternalIPAddress");

	    soapMessage.saveChanges();       	

	    return soapMessage;
	}
	
	
	/** Uptime **/
	public static SOAPMessage createSOAPRequest_GetStatusInfo() throws Exception {
	    MessageFactory messageFactory = MessageFactory.newInstance();
	    SOAPMessage soapMessage = messageFactory.createMessage();
	    SOAPPart soapPart = soapMessage.getSOAPPart();

	    SOAPEnvelope envelope = soapPart.getEnvelope();
	    envelope.addNamespaceDeclaration("u", "urn:schemas-upnp-org:service:WANIPConnection:1");

	    SOAPBody soapBody = envelope.getBody();
	    soapBody.addChildElement("GetStatusInfo", "u");

	    MimeHeaders headers = soapMessage.getMimeHeaders();
	    headers.addHeader("SOAPACTION", "urn:schemas-upnp-org:service:WANIPConnection:1#GetStatusInfo");

	    soapMessage.saveChanges();       	

	    return soapMessage;
	}
	
	
	
    /**
     * Method used to print the SOAP Response
     */
//    private static void printSOAPResponse(SOAPMessage soapResponse) throws Exception {
//        TransformerFactory transformerFactory = TransformerFactory.newInstance();
//        Transformer transformer = transformerFactory.newTransformer();
//        Source sourceContent = soapResponse.getSOAPPart().getContent();
//        System.out.print("\nResponse SOAP Message = ");
//        StreamResult result = new StreamResult(System.out);
//        transformer.transform(sourceContent, result);
//    }
}


