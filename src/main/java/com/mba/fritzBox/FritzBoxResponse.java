package com.mba.fritzBox;

import java.io.ByteArrayOutputStream;
import java.io.StringReader;

import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPMessage;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;

import com.mba.MPPTracker.Controller_MySQL;
import com.mba.MPPTracker.Structs.StMPPTrackerList;


public class FritzBoxResponse {
	private static Logger logger = LoggerFactory.getLogger(FritzBoxResponse.class.getName());
	private String IP = null;
	
	public FritzBoxResponse(String IP) {
		this.IP = IP;
	}

	public static void main(String[] args) {
		FritzBoxResponse inst = new FritzBoxResponse("fritz.box");
		StFritzboxDataTraffic result = new StFritzboxDataTraffic();
		inst.GetFritzboxDataDSLAM(result);
		result.setExternalIP(inst.GetFritzboxDataExternalIP());
		result.setUpTime(inst.GetFritzboxDataUptime());
		inst.GetFritzboxDataTraffic(result);
		
//		result.debug();
		
		new Controller_MySQL().addValue(result);
	}
	

	public StFritzboxDataTraffic getValue() {
		StFritzboxDataTraffic result = new StFritzboxDataTraffic();
		this.GetFritzboxDataDSLAM(result);
		result.setExternalIP(this.GetFritzboxDataExternalIP());
		result.setUpTime(this.GetFritzboxDataUptime());
		this.GetFritzboxDataTraffic(result);
		
//		result.debug();
		
		return result;
	}
	
	
	public String GetFritzboxDataMonitor(String Value){
    	String returnData = "";
//    	GetTotalBytesReceived
    	try {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
            String url = "http://" + IP + ":49000/igdupnp/control/WANCommonIFC1";
            SOAPMessage soapResponse = soapConnection.call(SOAP_Requests.createSOAPRequest_GetOnlineMonitor(Value), url);
            
            SAXBuilder builder = new SAXBuilder();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapResponse.writeTo(out);
            
            soapResponse.writeTo(System.out);
            
            Document doc = builder.build(new InputSource(new StringReader(out.toString())));
            Element rootElement = doc.getRootElement();
            Element BodyElement = rootElement.getChild("Body");
            Element IPAdressResponse = null;
            for(Element newElement : rootElement.getChildren()){
            	if (newElement.getName().equals("Body")){
            		BodyElement = newElement;
            	}
            	break;
            }
            
            for(Element newElement : BodyElement.getChildren()){
            	if (newElement.getName().equals("Get" + Value + "Response")){
            		IPAdressResponse = newElement;
            	}
            	break;
            }
            
            returnData = IPAdressResponse.getChildText("New" + Value);

            soapConnection.close();
        } catch (Exception e) {
            System.err.println("Error occurred while sending SOAP Request to Server");
            e.printStackTrace();
        }
    	logger.info(Value + ": " + returnData);
    	return returnData;
    }
    
	
	public String GetFritzboxDataExternalIP(){
    	String returnData = "";
    	
    	try {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
            String url = "http://" + IP + ":49000/igdupnp/control/WANIPConn1";
            SOAPMessage soapResponse = soapConnection.call(SOAP_Requests.createSOAPRequest_GetExternalIP(), url);
            
            SAXBuilder builder = new SAXBuilder();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapResponse.writeTo(out);
            
            if (logger.isTraceEnabled()){
            	soapResponse.writeTo(System.out);
            }
            
            Document doc = builder.build(new InputSource(new StringReader(out.toString())));
            Element rootElement = doc.getRootElement();
            Element BodyElement = rootElement.getChild("Body");
            Element IPAdressResponse = null;
            for(Element newElement : rootElement.getChildren()){
            	if (newElement.getName().equals("Body")){
            		BodyElement = newElement;
            	}
            	break;
            }
            
            for(Element newElement : BodyElement.getChildren()){
            	if (newElement.getName().equals("GetExternalIPAddressResponse")){
            		IPAdressResponse = newElement;
            	}
            	break;
            }
            
            returnData = IPAdressResponse.getChildText("NewExternalIPAddress");

            soapConnection.close();
        } catch (Exception e) {
            System.err.println("Error occurred while sending SOAP Request to Server");
            e.printStackTrace();
        }
    	logger.debug("ExternalIPAddress: " + returnData);
    	return returnData;
    }
    
	
	public long GetFritzboxDataUptime(){
    	try {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
            String url = "http://" + IP + ":49000/igdupnp/control/WANIPConn1";
            SOAPMessage soapResponse = soapConnection.call(SOAP_Requests.createSOAPRequest_GetStatusInfo(), url);
            
            SAXBuilder builder = new SAXBuilder();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapResponse.writeTo(out);
            
            if (logger.isTraceEnabled()){
            	soapResponse.writeTo(System.out);
            }
            
            Document doc = builder.build(new InputSource(new StringReader(out.toString())));
            Element rootElement = doc.getRootElement();
            Element BodyElement = rootElement.getChild("Body");
            for(Element newElement : rootElement.getChildren()){
            	if (newElement.getName().equals("Body")){
            		BodyElement = newElement;
            	}
            	break;
            }
            
            for(Element newElement : BodyElement.getChildren()){
            	if (newElement.getName().equals("GetStatusInfoResponse")){
            		BodyElement = newElement;
            	}
            	break;
            }
            
            soapConnection.close();
            
//            logger.debug("NewUptime: " + Long.parseLong(BodyElement.getChildText("NewUptime")));
            
            return Long.parseLong(BodyElement.getChildText("NewUptime"));
        } catch (Exception e) {
            System.err.println("Error occurred while sending SOAP Request to Server");
            e.printStackTrace();
        }
    	return 0;
    }
    
	
	public void GetFritzboxDataTraffic(StFritzboxDataTraffic result){
    	try {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
            String url = "http://" + IP + ":49000/igdupnp/control/WANCommonIFC1";
            SOAPMessage soapResponse = soapConnection.call(SOAP_Requests.createSOAPRequest_GetOnlineMonitor("AddonInfos"), url);
            
            SAXBuilder builder = new SAXBuilder();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapResponse.writeTo(out);
            
            if (logger.isTraceEnabled()){
            	soapResponse.writeTo(System.out);
            }
            
            Document doc = builder.build(new InputSource(new StringReader(out.toString())));
            Element rootElement = doc.getRootElement();
            Element BodyElement = rootElement.getChild("Body");

            for(Element newElement : rootElement.getChildren()){
            	if (newElement.getName().equals("Body")){
            		BodyElement = newElement;
            	}
            	break;
            }
            
            for(Element newElement : BodyElement.getChildren()){
            	if (newElement.getName().equals("GetAddonInfosResponse")){
            		BodyElement = newElement;
            	}
            	break;
            }
            
            result.setNewTotalBytesReceived(Long.parseLong(BodyElement.getChild("NewTotalBytesReceived").getText()));
            result.setNewTotalBytesSent(Long.parseLong(BodyElement.getChild("NewTotalBytesSent").getText()));
            
//            logger.debug("NewTotalBytesSent: "      + Long.parseLong(BodyElement.getChild("NewTotalBytesSent").getText()));
//            logger.debug("NewTotalBytesReceived: "  + Long.parseLong(BodyElement.getChild("NewTotalBytesReceived").getText()));

            soapConnection.close();
        } catch (Exception e) {
            System.err.println("Error occurred while sending SOAP Request to Server");
            e.printStackTrace();
        }
    }
	
	
	public void GetFritzboxDataDSLAM(StFritzboxDataTraffic result){
    	try {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
            String url = "http://" + IP + ":49000/igdupnp/control/WANCommonIFC1";
            SOAPMessage soapResponse = soapConnection.call(SOAP_Requests.createSOAPRequest_GetOnlineMonitor("CommonLinkProperties"), url);
            
            SAXBuilder builder = new SAXBuilder();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapResponse.writeTo(out);
            
            if (logger.isTraceEnabled()){
            	soapResponse.writeTo(System.out);
            }
            
            Document doc = builder.build(new InputSource(new StringReader(out.toString())));
            Element rootElement = doc.getRootElement();
            Element BodyElement = rootElement.getChild("Body");

            for(Element newElement : rootElement.getChildren()){
            	if (newElement.getName().equals("Body")){
            		BodyElement = newElement;
            	}
            	break;
            }
            
            for(Element newElement : BodyElement.getChildren()){
            	if (newElement.getName().equals("GetCommonLinkPropertiesResponse")){
            		BodyElement = newElement;
            	}
            	break;
            }
            
            result.setNewLayer1DownstreamMaxBitRate(Long.parseLong(BodyElement.getChild("NewLayer1DownstreamMaxBitRate").getText()));
            result.setNewLayer1UpstreamMaxBitRate(Long.parseLong(BodyElement.getChild("NewLayer1UpstreamMaxBitRate").getText()));
            
//            logger.info("NewLayer1UpstreamMaxBitRate: "    + Long.parseLong(BodyElement.getChild("NewLayer1UpstreamMaxBitRate").getText()));
//            logger.info("NewLayer1DownstreamMaxBitRate: "  + Long.parseLong(BodyElement.getChild("NewLayer1DownstreamMaxBitRate").getText()));

            soapConnection.close();
        } catch (Exception e) {
            System.err.println("Error occurred while sending SOAP Request to Server");
            e.printStackTrace();
        }
    }

}
