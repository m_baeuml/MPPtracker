package com.mba.fritzBox;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StFritzboxDataTraffic {
	private static Logger logger = LoggerFactory.getLogger(StFritzboxDataTraffic.class.getName());
	
	private String externalIP = null;
	private Long upTime = null;
	private Long NewTotalBytesSent = null;
	private Long NewTotalBytesReceived = null;
	private Long NewLayer1UpstreamMaxBitRate = null;
	private Long NewLayer1DownstreamMaxBitRate = null;


	public void debug() {
		logger.info("external IP" + "\t\t" + externalIP);
		logger.info("upTime" + "\t\t" + upTime);
		logger.info("NewTotalBytesSent" + "\t" + NewTotalBytesSent);
		logger.info("NewTotalBytesReceived" + "\t" + NewTotalBytesReceived);
		logger.info("NewLayer1UpstreamMaxBitRate" + "\t" + NewLayer1UpstreamMaxBitRate);
		logger.info("NewLayer1DownstreamMaxBitRate" + "\t" + NewLayer1DownstreamMaxBitRate);
	}
	
	public Long getNewTotalBytesSent() {
		return NewTotalBytesSent;
	}
	public void setNewTotalBytesSent(Long newTotalBytesSent) {
		NewTotalBytesSent = newTotalBytesSent;
	}
	public Long getNewTotalBytesReceived() {
		return NewTotalBytesReceived;
	}
	public void setNewTotalBytesReceived(Long newTotalBytesReceived) {
		NewTotalBytesReceived = newTotalBytesReceived;
	}
	public Long getNewLayer1UpstreamMaxBitRate() {
		return NewLayer1UpstreamMaxBitRate;
	}
	public void setNewLayer1UpstreamMaxBitRate(Long newLayer1UpstreamMaxBitRate) {
		NewLayer1UpstreamMaxBitRate = newLayer1UpstreamMaxBitRate;
	}
	public Long getNewLayer1DownstreamMaxBitRate() {
		return NewLayer1DownstreamMaxBitRate;
	}
	public void setNewLayer1DownstreamMaxBitRate(Long newLayer1DownstreamMaxBitRate) {
		NewLayer1DownstreamMaxBitRate = newLayer1DownstreamMaxBitRate;
	}
	public String getExternalIP() {
		return externalIP;
	}
	public void setExternalIP(String externalIP) {
		this.externalIP = externalIP;
	}
	public Long getUpTime() {
		return upTime;
	}
	public void setUpTime(Long upTime) {
		this.upTime = upTime;
	}
}
